from uuid import uuid4
from cryptocode import encrypt, decrypt
from re import split as RegSplit
from typing import Literal
from datetime import datetime
from yaml import safe_load as LoadYML

ONE_MONTH = (24 * 60 * 60 * 1000) * 31

def VerifyDetails(Username: str, Password: str) -> bool:
    with open(f"__users__/{Username}.yml") as file:
        DecryptedFromLogin = decrypt(LoadYML(file.read())["EncLogin"], Password)
        return not not DecryptedFromLogin

def UserLogin(Username: str, Password: str) -> str | Literal[False]:
    if not VerifyDetails(Username, Password):
        return False
    UUID = str(uuid4())
    with open("__users__/ActiveTokens.server.txt", "a") as file:
        file.write(encrypt(Username, UUID) + "&*DATE:" + str(datetime.now().timestamp()))
    return UUID

def UserToken(UUID: str) -> str:
    with open("__users__/ActiveTokens.server.txt") as file:
        for line in RegSplit(r"\r?\n", file.read()):
            if line == "":
                continue
            CurLine = decrypt(line.split("&*DATE:")[0], UUID)
            if not CurLine:
                continue
            return CurLine
